
/* ============================================================
 * Dealfish.js
 *
 * ============================================================ */
var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry10: function() {
        return navigator.userAgent.match(/BB10/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    iPod: function() {
        return navigator.userAgent.match(/iPod/i);
    },
    iPhone: function() {
        return navigator.userAgent.match(/iPhone/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    },
    anyM: function() {
        return ((isMobile.Android() && $(window).width() < 639) || isMobile.BlackBerry() || isMobile.iPod() || isMobile.iPhone() || isMobile.Opera() || isMobile.Windows());
    }
};
/*================ document ready =================*/
$(document).ready(function() {

    try {
        if (isMobile.iOS()) {
            $("body").addClass("ios");
        } else if (isMobile.Android()) {
            $("body").addClass("android");
        } else if (isMobile.BlackBerry()) {
            $("body").addClass("bb");
        } else if (isMobile.BlackBerry10()) {
            $("body").addClass("bb10");
        } else if (isMobile.Opera()) {
            $("body").addClass("opera");
        } else if (isMobile.Windows()) {
            $("body").addClass("windows");
        }
    } catch (e) {

    }

    /** ToogleSugNav **/
    try {
        toggleSuggNav.execute();
    } catch (e) {
        // 
    }

    try {
        if (isMobile.any()) {
            $("body").addClass("detect-mobile");
            $("#active-new-post").click(function() {
                $("#main-search-dealfish").toggleClass("show-search");
                $("body").toggleClass("active_h");
            });
        }
        if (!isMobile.any()) {
            $("body").removeClass("detect-mobile");
        }
    } catch (e) {

    }

    try {
        if (isMobile.any()) {
            var list_h = $("#main-navigation+div > div").length;
            if (list_h > 2) {
                $(".browse-cat .container>section>#tab-nav-category").css("margin-top", "130px");
            } else {
                $(".browse-cat .container>section>#tab-nav-category").css("margin-top", "92px");
            }
        }
    } catch (e) {
        // 
    }

    $(".nav-mobile").click(function() {
        $("#section-view-desktop").toggleClass("sub-menu");
        $(".show-nav-sub-menu").toggleClass("show");
    });

    /*============ hover sub nav menu product detail ===========*/
    $("#main_nav li a.focus").hover(function() {
        $("#main_nav li a.focus").removeClass("active");
        $(this).addClass("active");
        $(this).queue(function() {
            //$(".sub_nav").fadeOut();
            //$(this).siblings(".sub_nav").fadeIn();
            $(".sub_nav").hide();
            $(this).siblings(".sub_nav").show();
            $(this).dequeue();
        });
    });

    /*======= jQuery option =====*/
    if (!isMobile.any()) {
        $(".chzn-select").chosen();
        $(".chzn-select-deselect").chosen({
            allow_single_deselect: true
        });
        //$(".new-post").addClass('hidden-phone');

        $(".back-to-top").click(function() {
            $('body,html').animate({
                scrollTop: 0
            }, 1000);
            return false;
        });
    }
    if (isMobile.any() && $(window).width() > 767) {
        //$(".categories-list a.df-category").removeAttr("href");
    }
    var ieb = navigator.appVersion.indexOf("MSIE") != -1;
    /*
    if(!isMobile.any() && !ieb){     
        $(window).scroll(function() {
			if(!class_post){
				if (($(this).scrollTop() > 90) && ($(window).width() > 960) || $.browser.msie){
					$('#main-header').addClass("walk");
					$('.ui-autocomplete').addClass('fixedResult');
					$('.back-to-top').fadeIn();
				}else{
					$('#main-header').removeClass("walk");
					$('.ui-autocomplete').removeClass('fixedResult');
					$('.back-to-top').fadeOut();
				}
			}
		});
        $(window).on("resize scroll", function(e){
			if(!class_post){
				if($(window).width() < 960 || $(this).scrollTop() < 90 || $.browser.msie){
					$("#main-header").removeClass("walk");
					$('.ui-autocomplete').removeClass('fixedResult');
				}else if($(window).width() >= 960 || $(this).scrollTop() > 90 || $.browser.msie){
					$("#main-header").addClass("walk");
					$('.ui-autocomplete').addClass('fixedResult');
				}
			}
            set_posi_title();
        });
        function set_posi_title() {
            var _posiTop;
            if ($(window).width() < 960)
                _posiTop = 0;
            else
                _posiTop = 58;
            $('#fix-header').css("top", _posiTop);
        }
    } else {
        $('.feedback').addClass('hide');
    }
    */
    if (isMobile.any()) {
        $('.feedback').addClass('hide');
    }
    if ($.browser.msie && $.browser.version < 9) {
        $(".ie-show .btn-close-box").click(function(e) {
            e.preventDefault();
            $(".ie-show").hide();
            setCookie('ie_show_alert', '1', 7);
        });
    }
    if ($.browser.msie && $.browser.version < 9) {
        if (getCookie('ie_show_alert') == 1) {
            $(".ie-show").hide();
        }
    }
});

function dumpLOG(s) {
    try {
        //console.log(s);
    } catch (e) {
        // print e;
    }
}

function check_localize(mobile_url, path_url) {
    pattern_querystirng = /\?([^!])+/g
    pattern_chaingmai = /^(http[s]?\:\/\/)?(chiangmai)/g;
    pattern_khonkaen = /^(http[s]?\:\/\/)?(khonkaen)/g;
    var linkurl;
    if (pattern_chaingmai.test(path_url)) {
        linkurl = pattern_querystirng.test(mobile_url) ? mobile_url + '&pv=1' : mobile_url + '?pv=1';
        //console.log(linkurl);
    } else if (pattern_khonkaen.test(path_url)) {
        linkurl = pattern_querystirng.test(mobile_url) ? mobile_url + '&pv=13' : mobile_url + '?pv=13';
        //console.log(linkurl);
    } else {
        linkurl = mobile_url;
    }
    return linkurl;
}
/* end if */
//]]>


/*member*/
$.extend({
    URLEncode: function(e) {
        var t = "";
        var n = 0;
        e = e.toString();
        var r = /(^[a-zA-Z0-9_.]*)/;
        while (n < e.length) {
            var i = r.exec(e.substr(n));
            if (i != null && i.length > 1 && i[1] != "") {
                t += i[1];
                n += i[1].length
            } else {
                if (e[n] == " ") t += "+";
                else {
                    var s = e.charCodeAt(n);
                    var o = s.toString(16);
                    t += "%" + (o.length < 2 ? "0" : "") + o.toUpperCase()
                }
                n++
            }
        }
        return t
    },
    URLDecode: function(e) {
        var t = e;
        var n, r;
        var i = /(%[^%]{2})/;
        while ((m = i.exec(t)) != null && m.length > 1 && m[1] != "") {
            b = parseInt(m[1].substr(1), 16);
            r = String.fromCharCode(b);
            t = t.replace(m[1], r)
        }
        return t
    }
});

sanookmember = {
    FormRender: "",
    _chk: "0",
    _status: "offline",
    _registerUrl: "#",
    _authenurl: "/memberlogin",
    _deauthenurl: "/member/logout",
    _smiid: "",
    _provider: "",
    _autologin: "1",
    _mail: "",
    _option: "",
    _psnid: "000001-000000-000000-000000",
    _surl: "",
    _turl: "mweb",
    _lg: "t",
    _nickname: "",
    _onlineurl: "http://member.sanook.com/member_detail/change_profile_v2.aspx",
    _onlinetext: "",
    _offlineurl: "http://member.sanook.com/member_detail/disclaimer.aspx",
    _authenimageurl: "http://login.sanook.com/images/lg.gif",
    _deauthenimageurl: "http://login.sanook.com/images/lo.gif",
    _statupdateurl: "http://login.sanook.com/smistatupdate/smistatupdate.aspx",
    _authentype: "op",
    _login_text: "เน€เธ�เน�เธฒเธชเธนเน�เธฃเธฐเธ�เธ�",
    _logout_text: "เธญเธญเธ�เธ�เธฒเธ�เธฃเธฐเธ�เธ�",
    _display_text_type: 1,
    _urlavartarimage: "http://getimage.member.sanook.com/getimage.php?avatartype=3&gender=1&imgurl=http://member.sanook.com/sanookmemberimage/",
    _mclock: "",
    WriteInterface: function(e) {
        if (e == "online") {
            var displaynameMem = decodeURIComponent(this.getsubcookie("smiservice", "DISPLAYNAME").replace('+',' '));
            var l_df = ['<a href="/member/profile/" class="btn">'+ displaynameMem +'</a>', '<a href="/member/listing/" title="เธฃเธฒเธขเธ�เธฒเธฃเธ�เธฃเธฐเธ�เธฒเธจ" class="btn">เธฃเธฒเธขเธ�เธฒเธฃเธ�เธฃเธฐเธ�เธฒเธจ</a>', '<a href="/history/" title="เธฃเธฒเธขเธ�เธฒเธฃเธ—เธตเน�เธ”เธนเธฅเน�เธฒเธชเธธเธ”" class="btn">เธฃเธฒเธขเธ�เธฒเธฃเธ—เธตเน�เธ”เธนเธฅเน�เธฒเธชเธธเธ”</a>', '<a href="/member/favorite/" title="เธฃเธฒเธขเธ�เธฒเธฃเน�เธ�เธฃเธ”" class="btn">เธฃเธฒเธขเธ�เธฒเธฃเน�เธ�เธฃเธ”</a>', '<a href="' + this._deauthenurl + "?&surl=" + $.URLEncode(this._surl) + "&lg=" + this._lg + '" title="' + this._logout_text + '" title="เธญเธญเธ�เธ�เธฒเธ�เธฃเธฐเธ�เธ�" class="btn">' + this._logout_text + '</a>'];

            var lmoblie_df = ['<a href="/member/" class="btn">เธกเธธเธกเธชเธกเธฒเธ�เธดเธ�</a>', '<a href="/member/listing/" title="เธฃเธฒเธขเธ�เธฒเธฃเธ�เธฃเธฐเธ�เธฒเธจ" class="btn">เธฃเธฒเธขเธ�เธฒเธฃเธ�เธฃเธฐเธ�เธฒเธจ</a>', '<a href="/history/" title="เธฃเธฒเธขเธ�เธฒเธฃเธ—เธตเน�เธ”เธนเธฅเน�เธฒเธชเธธเธ”" class="btn">เธฃเธฒเธขเธ�เธฒเธฃเธ—เธตเน�เธ”เธนเธฅเน�เธฒเธชเธธเธ”</a>', '<a href="/member/favorite/" title="เธฃเธฒเธขเธ�เธฒเธฃเน�เธ�เธฃเธ”" class="btn">เธฃเธฒเธขเธ�เธฒเธฃเน�เธ�เธฃเธ”</a>', '<a href="/product/post/">เธฅเธ�เธ�เธฃเธฐเธ�เธฒเธจ</a>'];

            var t = "";
            t2 = '';
            for (var idf = 0; idf < l_df.length; idf++) {
                t += l_df[idf];
            };
            for (var idf = 0; idf < lmoblie_df.length; idf++) {
                t2 += '<li>' + lmoblie_df[idf] + '</li>';
            }
            t2 += '<li><a class="btn-register" href="' + this._deauthenurl + "?&surl=" + $.URLEncode(this._surl) + "&lg=" + this._lg + '" title="' + this._logout_text + '" title="เธญเธญเธ�เธ�เธฒเธ�เธฃเธฐเธ�เธ�" class="btn">' + this._logout_text + '</a></li>';
            var t2n = t2.replace(/class="btn"/gi, '');
            $('.section-top-bar .register').html(t);
            $('.show-nav-sub-menu .sub-list').html('<li><a href="/">เธ�เธฅเธฑเธ�เธซเธ�เน�เธฒเน�เธฃเธ�</a></li><li><a href="/all_category.html">เธซเธกเธงเธ”เธซเธกเธนเน�เธ—เธฑเน�เธ�เธซเธกเธ”</a></li>' + t2n);
            $('.section-top-bar .register').addClass('login-active');
            $('#home-aside .rg-member').hide();
            this._chk = "1";
            var n = 1;
            var r = setInterval(function() {
                i()
            }, 1e3);

            function i() {
                var e = $('.section-top-bar .register').html();
                if (e.match(/เธญเธญเธ�เธ�เธฒเธ�เธฃเธฐเธ�เธ�/g)) {
                    clearInterval(r);
                    //console.log("sanookmember OBJ : clearInterval Now!!")
                } else if (n > 4) {
                    clearInterval(r);
                    //console.log("sanookmember OBJ : count_c > 4 clearInterval Now!!")
                } else {
                    $(".welcome-user, .regiter").html(t);
                    n++
                }
            }
        }
    },
    RenderControl: function() {
        if (this.checkonlinestatus() == false) {} else {
            this._status = "online", this.WriteInterface("online")
        }
    },
    checkonlinestatus: function() {
        var e = this.getsubcookie("smiservice", "SMI_ID");
        var t = this.getsubcookie("smiservice", "EMAIL");
        var n = decodeURIComponent(this.getsubcookie("smiservice", "DISPLAYNAME"));
        this._nickname = n;
        this._email = t;
        this._smiid = e;
        if (e != "" && t != "" && e.length == 36) {
            return true
        } else {
            if (this._authentype == "md") document.location = $.URLEncode(this._authenurl) + "?psnid=" + this._psnid + "&surl=" + $.URLEncode(this._surl) + "&turl=" + $.URLEncode(this._turl) + "&lg=" + this._lg;
            return false
        }
    },
    smi_online_control_setsmiconfig: function(e, t, n, r) {
        if (e != "") this._psnid = e;
        if (t != "") this._surl = $.URLEncode(t.toString());
        if (e != "") this._lg = n;
        if (r != "") this._authentype = r;
        this._turl = "mweb"
    },
    getcookie: function(e) {
        var t = "" + document.cookie;
        var n = t.indexOf(e);
        if (n == -1 || e == "") return "";
        var r = t.indexOf(";", n);
        if (r == -1) r = t.length;
        return unescape(t.substring(n + e.length + 1, r))
    },
    getsubcookie: function(e, t) {
        var n = "" + document.cookie;
        var r = n.indexOf(e + "=");
        if (r == -1 || e == "") return "";
        var i = n.indexOf(";", r);
        if (i == -1) i = n.length;
        var s = unescape(n.substring(r + e.length + 1, i));
        if (t != "") {
            var o = s.indexOf(t);
            if (o == -1 || t == "") return "";
            var u = s.indexOf("&", o);
            if (u == -1) u = n.length;
            var a = s.substring(o + t.length + 1, u);
            return a
        } else {
            return s
        }
    }
};
var Base64 = {
    _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
    encode: function(e) {
        var t = "";
        var n, r, i, s, o, u, a;
        var f = 0;
        e = Base64._utf8_encode(e);
        while (f < e.length) {
            n = e.charCodeAt(f++);
            r = e.charCodeAt(f++);
            i = e.charCodeAt(f++);
            s = n >> 2;
            o = (n & 3) << 4 | r >> 4;
            u = (r & 15) << 2 | i >> 6;
            a = i & 63;
            if (isNaN(r)) {
                u = a = 64
            } else if (isNaN(i)) {
                a = 64
            }
            t = t + this._keyStr.charAt(s) + this._keyStr.charAt(o) + this._keyStr.charAt(u) + this._keyStr.charAt(a)
        }
        return t
    },
    decode: function(e) {
        var t = "";
        var n, r, i;
        var s, o, u, a;
        var f = 0;
        e = e.replace(/[^A-Za-z0-9\+\/\=]/g, "");
        while (f < e.length) {
            s = this._keyStr.indexOf(e.charAt(f++));
            o = this._keyStr.indexOf(e.charAt(f++));
            u = this._keyStr.indexOf(e.charAt(f++));
            a = this._keyStr.indexOf(e.charAt(f++));
            n = s << 2 | o >> 4;
            r = (o & 15) << 4 | u >> 2;
            i = (u & 3) << 6 | a;
            t = t + String.fromCharCode(n);
            if (u != 64) {
                t = t + String.fromCharCode(r)
            }
            if (a != 64) {
                t = t + String.fromCharCode(i)
            }
        }
        t = Base64._utf8_decode(t);
        return t
    },
    _utf8_encode: function(e) {
        e = e.replace(/\r\n/g, "\n");
        var t = "";
        for (var n = 0; n < e.length; n++) {
            var r = e.charCodeAt(n);
            if (r < 128) {
                t += String.fromCharCode(r)
            } else if (r > 127 && r < 2048) {
                t += String.fromCharCode(r >> 6 | 192);
                t += String.fromCharCode(r & 63 | 128)
            } else {
                t += String.fromCharCode(r >> 12 | 224);
                t += String.fromCharCode(r >> 6 & 63 | 128);
                t += String.fromCharCode(r & 63 | 128)
            }
        }
        return t
    },
    _utf8_decode: function(e) {
        var t = "";
        var n = 0;
        var r = c1 = c2 = 0;
        while (n < e.length) {
            r = e.charCodeAt(n);
            if (r < 128) {
                t += String.fromCharCode(r);
                n++
            } else if (r > 191 && r < 224) {
                c2 = e.charCodeAt(n + 1);
                t += String.fromCharCode((r & 31) << 6 | c2 & 63);
                n += 2
            } else {
                c2 = e.charCodeAt(n + 1);
                c3 = e.charCodeAt(n + 2);
                t += String.fromCharCode((r & 15) << 12 | (c2 & 63) << 6 | c3 & 63);
                n += 3
            }
        }
        return t
    }
}
/*member*/

    function clear_form_elements(ele) {
        $(ele).find(':input').each(function() {
            switch (this.type) {
                case 'password':
                case 'select-multiple':
                case 'select-one':
                case 'text':
                case 'textarea':
                    $(this).val('');
                    break;
                case 'checkbox':
                case 'radio':
                    this.checked = false;
            }
        });
    }


    function addBookmark() {
        var title = $("title").text();
        var url = window.location;
        if (window.sidebar) { /* Firefox*/
            window.sidebar.addPanel(title, url, '');
        } else if (window.opera) { /*Opera*/
            var a = document.createElement("A");
            a.rel = "sidebar";
            a.target = "_search";
            a.title = title;
            a.href = url;
            a.click();
        } else if (document.all) { /*IE*/
            window.external.AddFavorite(url, title);
        } else if (navigator.userAgent.toLowerCase().indexOf('chrome') > -1) {
            alert('เธ�เธ” ctrl+D เน€เธ�เธทเน�เธญ bookmark (เธซเธฃเธทเธญ เธ�เธ” Command+D เธชเธณเธซเธฃเธฑเธ� เน€เธ�เธฃเธทเน�เธญเธ� macs) เธ�เธฒเธ�เธ�เธฑเน�เธ� เธ�เธ” เธ•เธ�เธฅเธ�');
        }
    }

    /***Cookie */
    function getCookie(c_name) {
        var i, x, y, ARRcookies = document.cookie.split(";");
        for (i = 0; i < ARRcookies.length; i++) {
            x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
            y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
            x = x.replace(/^\s+|\s+$/g, "");
            if (x == c_name) {
                return unescape(y);
            }
        }
    }

    function setCookie(c_name, value, exdays) {
        var exdate = new Date();
        exdate.setDate(exdate.getDate() + exdays);
        var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
        var path = '/';
        var domain = '.dealfish.co.th';
        document.cookie = c_name + "=" + c_value + ((path) ? ";path=" + path : "") + ((domain) ? ";domain=" + domain : "");
        //document.cookie = c_name + "=" + c_value + ((expires ) ? ";expires=" + expires_date.toGMTString() : "" ) + ((path ) ? ";path=" + path : "" ) + ((domain ) ? ";domain=" + domain : "" ) + ((secure ) ? ";secure" : "" );

    }

    /**
     * get query string
     */
    function GetValueQueryString(key, default_) {
        if (default_ == null) default_ = "";
        key = key.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regex = new RegExp("[\\?&]" + key + "=([^&#]*)");
        var qs = regex.exec(window.location.href);
        if (qs == null) return default_;
        else return qs[1];
    }


    function cate_bar_box(selected, ele) {
        var parent_cate = '';
        for (var i = 0; i < cate[0].child.length; i++) {
            parent_cate += '<option value="' + cate[0].child[i].cate_id + '" class="df-opt-parent">' + cate[0].child[i].cate_name + '<\/option>';
            if (cate[cate[0].child[i].cate_id].child) {
                for (var j = 0; j < cate[cate[0].child[i].cate_id].child.length; j++) {
                    parent_cate += '<option value="' + cate[cate[0].child[i].cate_id].child[j].cate_id + '">' + cate[cate[0].child[i].cate_id].child[j].cate_name + '<\/option>';
                }
            }
        }
        try { //to fixed ie6 error
            $('select[name="' + ele + '"]').append(parent_cate).val(selected);
        } catch (err) {}
        $(ele).chosen({
            no_results_text: "เน�เธกเน�เธ�เธ�เธซเธกเธงเธ”เธซเธกเธนเน�เธ—เธตเน�เธ�เน�เธ�เธซเธฒ"
        });
    }

    function menu_tracking(size) {
        try {
            if (window._tracking_page == undefined) return false;
            /*if(size=='w'){
         $('#main-header #main-navigation').find('a').attr({ onclick: "_gaq.push(['_trackEvent', 'Nav-Menu', '"+window._tracking_page+"', 'Category-Show']);" });
         $('#main-header #localize-nav').find('a').attr({ onclick: "_gaq.push(['_trackEvent', 'Nav-Menu', '"+window._tracking_page+"', 'Province-Show']);" });
         }else{*/
            $('#main-header #main-navigation').find('a').attr({
                onclick: "_gaq.push(['_trackEvent', 'Nav-Menu', '" + window._tracking_page + "', 'Category-Hide']);"
            });
            $('#main-header #localize-nav').find('a').attr({
                onclick: "_gaq.push(['_trackEvent', 'Nav-Menu', '" + window._tracking_page + "', 'Province-Hide']);"
            });
            //}
        } catch (e) {
            //console.log(e);
        }
    }

$('#detail, #name, #fb_email').live('focus', function() {
    if (!$(this).hasClass('cleared')) {
        $(this).addClass('cleared').val('');
    }
});

$('#fb_email').live('focusout', function() {
    if (!/[a-z0-9]+@[a-z]+\.[a-z]+/.test(this.value) && this.value != '') {
        alert('เธ�เธฃเธธเธ“เธฒเธ�เธฃเธญเธ�เธญเธตเน€เธกเธฅเน�เธซเน�เธ–เธนเธ�เธ•เน�เธญเธ�เธ”เน�เธงเธขเธ�เน�เธฐ');
        $(this).focus();
        return false;
    }
});
$('#fb_submit1').live('click', function() {
    if (!$('#detail.cleared').length || $('#detail').val().replace(/\s/ig, '') == '') {
        return false;
    }
    //alert($('#detail.cleared').val()); return false;

    $('#fb_submit1').attr('disabled', true);
    $('#closefeedback').attr('disabled', true);
    var detail = $('#detail').val(),
        name = $('#name').val(),
        email = $('#fb_email').val();
    $.post('/welcome/send_feedback/', {
        detail: $('#detail').hasClass('cleared') ? detail : '-',
        name: $('#name').hasClass('cleared') ? name : '-',
        email: $('#fb_email').hasClass('cleared') ? email : '-'
    },

    function(res) {
        $('#detail').removeClass('cleared').val('เธ�เธดเธกเธ�เน�เธ�เน�เธญเธ�เธงเธฒเธกเธ�เธญเธ�เธ�เธธเธ“เธ—เธตเน�เธ�เธตเน�เธ�เน�เธฐ');
        $('#name').removeClass('cleared').val('เน�เธ�เธฃเธ”เธฃเธฐเธ�เธธเธ�เธทเน�เธญเธ�เธญเธ�เธ�เธธเธ“ (เธ�เธฃเธญเธ�เธซเธฃเธทเธญเน�เธกเน�เธ�เน�เน�เธ”เน�)');
        $('#fb_email').removeClass('cleared').val('เน�เธ�เธฃเธ”เธฃเธฐเธ�เธธ เธญเธตเน€เธกเธฅเน� เธซเธฃเธทเธญ เน€เธ�เธญเธฃเน�เน�เธ—เธฃเธ•เธดเธ”เธ•เน�เธญ (เธ�เธฃเธญเธ�เธซเธฃเธทเธญเน�เธกเน�เธ�เน�เน�เธ”เน�)');
        $('#fb_submit1').removeAttr('disabled');
        $('#closefeedback').removeAttr('disabled');
        $.fancybox(res);
    });
});
$('#closefeedback').live('click', function() {
    $('#detail').removeClass('cleared').val('เธ�เธดเธกเธ�เน�เธ�เน�เธญเธ�เธงเธฒเธกเธ�เธญเธ�เธ�เธธเธ“เธ—เธตเน�เธ�เธตเน�เธ�เน�เธฐ');
    $('#name').removeClass('cleared').val('เน�เธ�เธฃเธ”เธฃเธฐเธ�เธธเธ�เธทเน�เธญเธ�เธญเธ�เธ�เธธเธ“ (เธ�เธฃเธญเธ�เธซเธฃเธทเธญเน�เธกเน�เธ�เน�เน�เธ”เน�)');
    $('#fb_email').removeClass('cleared').val('เน�เธ�เธฃเธ”เธฃเธฐเธ�เธธ เธญเธตเน€เธกเธฅเน� เธซเธฃเธทเธญ เน€เธ�เธญเธฃเน�เน�เธ—เธฃเธ•เธดเธ”เธ•เน�เธญ (เธ�เธฃเธญเธ�เธซเธฃเธทเธญเน�เธกเน�เธ�เน�เน�เธ”เน�)');
    $('#fb_submit1').removeAttr('disabled');
    $('#closefeedback').removeAttr('disabled');
    $.fancybox.close();
    return false;
});

var month = ['เธก.เธ�.', 'เธ�.เธ�.', 'เธกเธต.เธ�.', 'เน€เธก.เธข.', 'เธ�.เธ�.', 'เธกเธด.เธข.', 'เธ�.เธ�.', 'เธช.เธ�.', 'เธ�.เธข.', 'เธ•.เธ�.', 'เธ�.เธข.', 'เธ�.เธ�.'];

function display_post_date(val) {
    if (val == undefined) return 'เน�เธกเน�เธฃเธฐเธ�เธธ';
    var d1 = new Date(val);
    var today = new Date();
    var t2 = today.getTime();
    var t1 = d1.getTime();
    var diff = parseInt((t2 - t1) / (24 * 3600 * 1000));
    if (diff == 0) {
        return ' เธงเธฑเธ�เธ�เธตเน� ';
    } else if (diff == 1) {
        return ' เน€เธกเธทเน�เธญเธงเธฒเธ� ';
    } else {
        var tmpD = val.split(",");
        if (tmpD[2] == undefined) return 'เน�เธกเน�เธฃเธฐเธ�เธธ';
        return (tmpD[1]) + ' ' + month[tmpD[0] - 1] + ' ' + (parseFloat(tmpD[2]) + 543).toString().substring(2);
    }
}

function validEmail(email) {
    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    return filter.test(email);
}

function limitTextArea(c, b, a) {
    if (c.value.length > a) {
        c.value = c.value.substring(0, a)
    } else {
        b.value = a - c.value.length
    }
}

function clear_form_elements(ele) {
    $(ele).find(':input').each(function() {
        switch (this.type) {
            case 'password':
            case 'select-multiple':
            case 'select-one':
            case 'text':
            case 'textarea':
                $(this).val('');
                break;
            case 'checkbox':
            case 'radio':
                this.checked = false;
        }
    });
}
/**
 * toggle nav
 */
var toggleSuggNav = {
    cookie_is_toggle: '1',
    getD: function() {
        return this.cookie_is_toggle;
    },
    execute: function() {
        /** onClickListener **/
        toggleSuggNav.removeMyClass();
    },
    addMyClass: function() {
        if (!$(".df-icon-arrow").hasClass('df-expand')) {
            $(".min-nav").toggleClass("show-nav");
            $(".df-icon-arrow").toggleClass("df-expand");
            setCookie('ToggleSugNav', '1', 1);
        }
    },
    removeMyClass: function() {
        if ($(".df-icon-arrow").hasClass('df-expand')) {
            $(".min-nav").toggleClass("show-nav");
            $(".df-icon-arrow").toggleClass("df-expand");
            setCookie('ToggleSugNav', '0', 1);
        }
    }
}

/**
 * hot recently keyword
 * @modifier 12/07/2013
 * @author NitinaiD
 */
$(document).ready(function() {
    if (!isMobile.any()) {
        hotRecentlyKeyword.getCookieKeyword();
        if (hotRecentlyKeyword.sizeOfListCookieKeyword < 1) {
            hotRecentlyKeyword.getDefaultKeyword();
        } else {
            /** for cookie **/
            // remove cookie
            $('.stat-post-update span i').click(function() {
                var indexKey = $(this).attr('data-item');
                if (hotRecentlyKeyword.removeCookieKeyword(indexKey)) {
                    $('.stat-post-update span[data-item="' + indexKey + '"]').hide();
                }
            });

            // onclick cookie
            $('.stat-post-update span a').click(function() {
                FlagClickKeyword = true;
                $('form#search #q').val($(this).text());
                $('form#search').submit();
            });
        }

        $('form#search').submit(function() {
            if (($('#search #q').val() !== undefined && $('#search #q').val() != '') && FlagClickKeyword != true) {
                hotRecentlyKeyword.addCookieKeyword($('#search #q').val());
            }
        });
    }
});

// need cookie function
var hotRecentlyKeyword = {
    hrk_keyword: {},
    sizeOfListCookieKeyword: 0,
    getCookieKeyword: function() {
        var newHTML = '';
        var i = 1;
        try {
            var hrkCookieLasted = getCookie('hrk_search');
            var keywordSting = jQuery.parseJSON(hrkCookieLasted);
            for (var indexKeyHashtable in keywordSting) {
                if (i > 7) continue;
                if (keywordSting[indexKeyHashtable] === undefined || keywordSting[indexKeyHashtable] === '') continue;
                this.hrk_keyword[indexKeyHashtable] = keywordSting[indexKeyHashtable];
                newHTML = "<span data-item = \"" + indexKeyHashtable + "\" onclick=\"_gaq.push(['_trackEvent', 'KeywordSearch', 'UserKeyword', '" + this.hrk_keyword[indexKeyHashtable] + "']);\" ><a href=\"javascript:void(0);\">" + this.hrk_keyword[indexKeyHashtable] + "</a> <i data-item = \"" + indexKeyHashtable + "\">close</i></span>" + newHTML;
                i++;
            }
            this.getSizeCookieKeyword();
            $('.stat-post-update').html('<span>เธ�เธณเธ�เน�เธ�เธซเธฒเธฅเน�เธฒเธชเธธเธ”</span>' + newHTML);
        } catch (e) {

        }
    },
    getSizeCookieKeyword: function() {
        try {
            this.sizeOfListCookieKeyword = Object.keys(this.hrk_keyword).length;
        } catch (e) {

        }
    },
    addCookieKeyword: function(keyword_search) {
        var mc_key = new Date().getTime();
        var hrk_keyword = {};
        hrkCookieLasted = getCookie('hrk_search');
        if (hrkCookieLasted !== undefined) {
            hrk_keyword = jQuery.parseJSON(hrkCookieLasted);
        }

        try {
            // check เธ�เธณเธ�เน�เธณ
            for (var indexKeyHashtable in hrk_keyword) {
                if (hrk_keyword[indexKeyHashtable] == keyword_search) {
                    delete hrk_keyword[indexKeyHashtable];
                }
            }
        } catch (e) {

        }
        try {
            if (Object.keys(hrk_keyword).length < 7) {
                hrk_keyword[mc_key] = keyword_search;
                this.sizeOfListCookieKeyword = Object.keys(hrk_keyword).length;
                setCookie('hrk_search', JSON.stringify(hrk_keyword), 7);
            } else {
                var i = 1;
                for (var indexKeyHashtable in hrk_keyword) {
                    if (i > 1) continue;
                    delete hrk_keyword[indexKeyHashtable];
                    i++;
                }
                hrk_keyword[mc_key] = keyword_search;
                setCookie('hrk_search', JSON.stringify(hrk_keyword), 7);
                this.sizeOfListCookieKeyword = 7;
            }
        } catch (e) {}
        return true;
    },
    removeCookieKeyword: function(indexKey) {
        var hrk_keyword = {};
        hrkCookieLasted = getCookie('hrk_search');
        if (hrkCookieLasted !== undefined) {
            hrk_keyword = jQuery.parseJSON(hrkCookieLasted);
            if (hrk_keyword[indexKey] !== undefined) {
                delete hrk_keyword[indexKey];
            }
            if (hrk_keyword) {
                setCookie('hrk_search', JSON.stringify(hrk_keyword), 7);
            }
            return true;
        } else {
            return false;
        }
    },
    getDefaultKeyword: function() {
        var url = SITE_URL + 'recently/hot_recently_keyword/get_default_key/';
        var data = {
            cate_id: CATEGORY_ID
        };
        $.ajax({
            dataType: "json",
            url: url,
            data: data,
            success: function(json_data) {
                json_data;
                try {
                    var newHTML = '<span>เธ�เธณเธ�เน�เธ�เธซเธฒเน�เธ�เธฐเธ�เธณ</span>';
                    for (var objKey in json_data) {
                        newHTML = newHTML + "<span><a href=\"javascript:void(0);\" onclick=\"_gaq.push(['_trackEvent', 'KeywordSearch', 'AutoKeyword', '" + json_data[objKey] + "']);\">" + json_data[objKey] + "</a></span>";
                    }
                    $('.stat-post-update').html(newHTML);

                    /** onclick **/
                    $('.stat-post-update span a').click(function() {
                        FlagClickKeyword = true;
                        $('form#search #q').val($(this).text());
                        $('form#search').submit();
                    });
                } catch (e) {

                }
            }
        })
            .done(function() { /* console.log( "second success" ); */
        })
            .fail(function() { /* console.log( "error" ); */
        })
            .always(function() { /* console.log( "complete" ); */
        });
    }
}; 